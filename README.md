# Analysis I

*Since the course language is German, the content of this repository is also largely written in German. If you want to help translating, feel free to open a PR.*

In dieser Repository, versuche ich alle Dokumente, die ich im Laufe des Kurses "Analysis für Mathematiker I" erstelle, zu sammeln.
Darunter fallen unter anderem Vorlesungsmitschriften (sofern vorhanden), Übungsaufgaben und auch Hausaufgaben.

Jeder darf die hier veröffentlichten Inhalte gerne für seine Zwecke verwenden und falls man dabei auf einen Fehler oder eine Unstimmigkeit stößt, würde ich mich über eine Rückmeldung (EMail, PR,...) sehr freuen.
